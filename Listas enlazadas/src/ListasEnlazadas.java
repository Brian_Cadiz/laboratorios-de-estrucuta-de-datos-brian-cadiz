public class ListasEnlazadas {

    private Nodo laCabeza;

    ListasEnlazadas() {
        laCabeza = null;
    }
    //---Inserta un objeto(int) al comienzo de la lista
    public void InsertaInicio(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else  laCabeza = new Nodo(o, laCabeza);
    }

    //---- Inserta al final ----
    public void InsertaFinal(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else{
            Nodo t;
            for(t = laCabeza; t.next != null; t= t.next) ;;
            t.next = new Nodo(o,null);
        }
    }

    // ---cuenta la cantidad de nodos de la lista (Size)
    public int Size() {
        int tnodos=0;
        for(Nodo t = laCabeza; t !=null; t= t.next)  tnodos++;
        return tnodos;
    }

    //eliminar un nodo de la lista
// Elimina el primer nodo n tal que o.elObjeto==o
    public void Eliminar(int o) {
        if(!EstaVacia()) {
            if(laCabeza.elObjeto==o) laCabeza = laCabeza.next;
            else {
                Nodo p = laCabeza;
                Nodo t = laCabeza.next;
                while (t !=null && t.elObjeto != o)  {
                    p = t ; t = t.next;
                }
                if(t.elObjeto==o) p.next = t.next;
            }
        }
    }

    // Verifica si la lista está vacias;
    public boolean EstaVacia() {
        return laCabeza == null;
    }

    //para esta funcion se crean dos variables tipo int que son asinadas a el tamaño y el promedio y mas una variable tipo nodo a la cual la llame n y esta es igual a la raiz
    public int obtenerpromedio() {
   int tamaño=0;
   int promedio=0;
   Nodo n=laCabeza;

   //se crea un while el cual si la variable n es diferente de nulo entonces el promedio se suma con el objeto de n ,
        // el tamaño se aumenta y el n apuntaria hacia el siguiente nodo.
   while (n!=null){
       promedio+=n.elObjeto;
       tamaño++;
       n=n.next;
   }
   return promedio/tamaño;
    }

    //para la funcion de mayor alpromedio se debe de crear una variabe tipo clase que en este caso seria ListasEnlazadas ,
    // tambien se debe de crear un nodo que en este caso lo llame n el cual es igual a la raiz y se crea una variable tipo int
    // la cual la llame promedio la cual es igual a la funcion obtener promedio
    public  ListasEnlazadas mayoresPromedio(){
        ListasEnlazadas l=new ListasEnlazadas();
        Nodo n=laCabeza;
        int promedio=obtenerpromedio();
        //se crea un while el cual si n es diferente de nulo y el objeto de n es mayor que el promedio entonces se crea un booleano
        //el cual sera igual a la funcion de inserccion en orden y si esa variable tipo bolleana existe entonces la variable l se
        //insertara en el final y de ejecutarse todas estas condiciones entonces n apuntara asia el siguiente nodo y finalmente se retorna l.
        while(n!=null){
            if(n.elObjeto>promedio){
                boolean opcion=InsertarOrdenado(n);
                if(opcion){
                    l.InsertaFinal(n.elObjeto);
                }
            }
            n=n.next;
        }
        return l;
    }

    //esta funcion asignada para la inserccion ordenada es tipo booleana y tiene por funcion una varianle nodo la cual llame n
    // una variable tipo nodo a la cual llame x la cual es igual a la raiz
    public boolean InsertarOrdenado(Nodo n){
        Nodo x=laCabeza;
       //si el objeto de el nodo n es mayor que el de el nodo x entonces la direccion de el siguiente nod de x seria igual a la direccion de el nodo n
        //y entonces la puntera de el siguiente nodo de n seria igual a el nodo x , si esto sucede entonces se retorna verdadero.
        if(n.elObjeto>x.elObjeto){
            x.next=n.next;
            n.next=x;

            return  true;
        }else{
            n.next=x;
            return true;
        }
    }

    //-----Imprime la lista-----
    void Print() {
        if(laCabeza!=null) Imprimir(laCabeza);
        else System.out.println("Lista Vacia");
    }

    void Imprimir(Nodo m ) {
        if(m !=null) {
            m.Print(); Imprimir(m.next);}
    }




    //-----Clase Nodo---------
    private class Nodo {
        public int elObjeto;
        public Nodo next;
        public Nodo(int nuevoObjeto, Nodo next)
        {this.elObjeto=nuevoObjeto; this.next = next;}
        void Print(){ System.out.print("- " + elObjeto);}

    }

}
