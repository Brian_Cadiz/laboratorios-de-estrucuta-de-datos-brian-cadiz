public class TestMain {
    public static void main(String[] args) {
        int n=1000000;
        BSTree bst=new BSTree();
        AvlTree avl=new AvlTree();



        System.out.println(" inserccion BST VS AVL");

        //se crean dos variables tipo long que se atribuyen al tiempo de el BST y el AVL que se asignaran a la inserccion.
        long tiempoAvlInsercion=System.nanoTime();
        long tiempoBstInsercion=System.nanoTime();

       //se asigna un for para recorrer el BST y el AVL en funcion de los 100.000 elementos
        for (int i=0;i<n;i++){
            bst.search(i);
            avl.search(i);
        }
        long TiempofinalBstInsercion=System.nanoTime()-tiempoBstInsercion;
        System.out.println("Bst="+TiempofinalBstInsercion);

        long TiempoFinalAvlInsercion=System.nanoTime()-tiempoAvlInsercion;
        System.out.println("Avl="+TiempoFinalAvlInsercion);


        System.out.println(" Busqueda BST VS AVL ");

        //se crean dos variables tipo long que se atribuyen al tiempo de el BST y el AVL que se asignaran a la inserccion.
        long tiempoAvlBusqueda=System.nanoTime();
        long tiempoBstBusqueda=System.nanoTime();

        //se asigna un for para recorrer el BST y el AVL en funcion de los 100.000 elementos
        for (int i=0;i<n;i++){
            bst.search(i);
            avl.search(i);
        }
        long TiempofinalBstBusqueda=System.nanoTime()-tiempoBstBusqueda;
        System.out.println("Bst="+TiempofinalBstBusqueda);

        long TiempoFinalAvlBusqueda=System.nanoTime()-tiempoAvlBusqueda;
        System.out.println("Avl="+TiempoFinalAvlBusqueda);


    }
}
