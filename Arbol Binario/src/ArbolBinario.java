import java.util.LinkedList;

public class ArbolBinario {
    int dato;
    ArbolBinario iz;
    ArbolBinario der;
    public ArbolBinario(int dato){
        this.dato=dato;
        iz=null;
        der=null;
    }

    public ArbolBinario(int dato, ArbolBinario iz, ArbolBinario der){
        this.dato=dato;
        this.iz=iz;
        this.der=der;
    }
    public void preOrden(){
        System.out.print(this.dato+" ");
        if(iz!=null) iz.preOrden();
        if(der!=null) der.preOrden();
    }

    public void inOrden(){
        if(iz!=null) iz.preOrden();
        System.out.print(this.dato+" ");
        if(der!=null) der.preOrden();
    }

    public void posOrden(){
        if(iz!=null) iz.preOrden();
        if(der!=null) der.preOrden();
        System.out.print(this.dato+" ");
    }

    //cantidad de nodos-1 del camino más largo de la raiz a sus hojas
    public int altura(){
        return altura(this);
    }

    private int altura(ArbolBinario raiz){
        if(raiz==null){
            return -1;
        }
        return 1 + Math.max(altura(raiz.iz),altura(raiz.der));
    }

    //size: cantidad de nodos del árbol
    public int size(){
        return size(this);
    }

    private int size(ArbolBinario raiz){
        if (raiz==null){
            return 0;
        }
        return 1 + size(raiz.iz) + size(raiz.der);
    }

    public void tree(){
        tree(this, "");
    }

    //e crea un Linkedlist para guardar el rango ya que en la funcion rango se devolvera este linkedlist.
    LinkedList<ArbolBinario>rangoLista=new LinkedList<>();
    public LinkedList<ArbolBinario>rango(int i,int j){
        //se comprueba si este es que este nodo no equiste entonces se retornra un nuevo LinkedList ,
        //de lo contrario significa que existe este nodo por lo que se puede continuar con el procedimiento de la funcion.
        if(this==null ){
            return new LinkedList<>();

        }else {
            //como es una bisqueda de  rango se debe de verificar si la variable se encuentra entre el entero i y el entero j,
            //y de ser asi entonces este nodo se agrega al LinkedList.
            if (this.dato>=i && this.dato<=j ){
                rangoLista.add(this);
            }
            if (this.iz!=null){
                iz.rangoLista=this.rangoLista;
                rango(i,j);
            }else{
                if(this.iz==null) {
                    iz.rangoLista = null;
                }
            }
                if (this.der != null) {
                    der.rangoLista = this.rangoLista;
                    rango(i, j);
                }else{
                    if (this.der==null) {
                        der.rangoLista = null;
                    }
                }

            return rangoLista;
        }
    }

    private void tree(ArbolBinario raiz, String tab) {
        if(raiz!=null){
            System.out.println(tab+"->"+raiz.dato);
            tree(raiz.iz, tab+"  |");
            tree(raiz.der, tab+"  ");
        }
    }

}
