import java.util.Arrays;

public class main {

    public static void main(String[] args) {

        //se crea un int tipo unidimensional en eo cual se guardaran los tamaños asignados en el problema.
        int dato[] = new int[9];

        dato[0] = 1000;
        dato[1] = 5000;
        dato[2] = 10000;
        dato[3] = 50000;
        dato[4] = 100000;
        dato[5] = 1000000;
        dato[6] = 5000000;
        dato[7] = 10000000;
        dato[8] = 50000000;

        //se crea un long tipo bidimensional ya que en estos se iran a ir guardando los valores de BUbbleSort,MergeSort y JavaSort
        long data[][] = new long[9][3];


        System.out.println("Bubble Sort  -Merge Sort  -Java Sort");

        //se crea un for en el cual se recorrera el largo de los datos
        for (int i = 0; i < dato.length; i++) {

            int array1[] = MedicionTiempo.fillArray(dato[i]);
            int array2[] = MedicionTiempo.fillArray(dato[i]);

            int arr_1[] = array1.clone();
            int arr_2[] = array2.clone();
            int arr_3[] = array1.clone();
            int arr_4[] = array2.clone();

            //se crea una variable tipo long la cual sera estimada para el tiempo inicial u esta seria igual a el sistema de impresion para nanosegundos
            //luego se debe de verificar que el dato inicila sea mneor o igual a la sifra estimada que en este caso es 100000 y de ser asi entonces el array1 es añadido a bubbleSort de lo contrario en bubbleSort seria igual a 0.
            long tiempoInicio = System.nanoTime();
            if (dato[i] <= 100000) {
                MedicionTiempo.bubbleSort(array1);

                //se crea una variable tipo long la cual sera estimada para el tiempo inicial y esta sera igual a el sistema de nanosegundos y el tiempo inicial
                //luego se inicialisara la variable tipo data [0][0] en la cual se guardara el bubbleSort, la cual finalmente sera igual el data [0][0] mas el tiempo final dividido en 2.
                long tiempoFinal = System.nanoTime() - tiempoInicio;
                data[0][0] = tiempoFinal;
                tiempoInicio = System.nanoTime();

                MedicionTiempo.bubbleSort(array2);
                tiempoFinal = System.nanoTime() - tiempoInicio;
                data[0][0] = (data[0][0] + tiempoFinal) / 2;
            } else {
                data[0][0] = 0;
            }

            //la variable tipo long la cual sera estimada para el tiempo inicial  sera igual a el sistema de nanosegundos y el arr_1 se agregara a el mergeSort y despues el arr_3 sera agregado despues de ocupar el data [0][1], en esta funcion para mergeSort  el tiempo final sera igual a el sistema de nanosegundos y el tiempo inicial,
            // luego se inicialisara la variable tipo data [0][1] en la cual se guardara el mergeSort, la cual finalmente sera igual al data [0][1] mas el tiempo final dividido en 2.
            tiempoInicio = System.nanoTime();
            MedicionTiempo.mergeSort(arr_1);
            long tiempoFinal = System.nanoTime() - tiempoInicio;
            data[0][1] = tiempoFinal;

            tiempoInicio = System.nanoTime();
            MedicionTiempo.mergeSort(arr_3);
            tiempoFinal = System.nanoTime() - tiempoInicio;
            data[0][1] = (data[0][1] + tiempoFinal) / 2;

            //la variable tipo long la cual sera estimada para el tiempo inicial  sera igual a el sistema de nanosegundos y el arr_2 se agregara a el mergeSort y despues el arr_4 sera agregado despues de ocupar el data [0][2], en esta funcion para javaSort  el tiempo final sera igual a el sistema de nanosegundos y el tiempo inicial,
            // luego se inicialisara la variable tipo data [0][2] en la cual se guardara el javaSort, la cual finalmente sera igual al data [0][2] mas el tiempo final dividido en 2.
            tiempoInicio = System.nanoTime();
            Arrays.sort(arr_2);
            tiempoFinal = System.nanoTime() - tiempoInicio;
            data[0][2] = tiempoFinal;

            tiempoInicio = System.nanoTime();
            Arrays.sort(arr_4);
            tiempoFinal = System.nanoTime() - tiempoInicio;
            data[0][2] = (data[0][2] + tiempoFinal) / 2;

            System.out.println(data[0][0] + "  -   " + data[0][1] + "  -   " + data[0][2]);

        }
    }
}
