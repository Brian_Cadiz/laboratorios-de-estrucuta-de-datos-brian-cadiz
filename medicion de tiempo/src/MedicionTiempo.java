import java.util.*;



public class MedicionTiempo {
    public static int[] fillArray(int n) {

        int[] array = new int[n];
        Random rnd = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rnd.nextInt(n * 10);
        }
        return array;
    }

    public static void bubbleSort(int array[]) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    // intercambiar arr[j+1] y arr[j]
                    int temporal = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temporal;
                }
            }
        }
    }

    public static void Union(int array[], int l, int m, int r) {

        int n1 = m - l + 1;
        int n2 = r - m;


        int L[] = new int[n1];
        int R[] = new int[n2];


        for (int i = 0; i < n1; ++i)
            L[i] = array[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = array[m + 1 + j];


        int i = 0, j = 0;


        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                array[k] = L[i];
                i++;
            } else {
                array[k] = R[j];
                j++;
            }
            k++;
        }


        while (i < n1) {
            array[k] = L[i];
            i++;
            k++;
        }


        while (j < n2) {
            array[k] = R[j];
            j++;
            k++;
        }
    }

    // Función principal que ordena arr[l..r] usando
    // fusionar()
    public static void clasificar(int array[], int l, int r) {
        if (l < r) {
            // Encuentra el punto medio
            int m = l + (r - l) / 2;

            // Ordenar la primera y la segunda mitad
            clasificar(array, l, m);
            clasificar(array, m + 1, r);

            // Combinar las mitades ordenadas
            Union(array, l, m, r);
        }
    }

    public static void mergeSort(int array[]) {
        clasificar(array, 0, array.length - 1);
    }

    public static void print(int array[]) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }


}
