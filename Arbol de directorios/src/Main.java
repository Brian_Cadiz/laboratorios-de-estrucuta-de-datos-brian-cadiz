public class Main {
    public static void main(String[] args) {
        // TODO stub de método generado automáticamente
        ArbolDirectorio ad = new ArbolDirectorio();


            ad.Imprimir();
            System.out.println("Tamaño actual: " + ad.size());


            ad.crearCarpeta("raiz", "Carpeta 1");
            ad.crearCarpeta("raiz", "Carpeta 2");
            ad.crearCarpeta("raiz", "Carpeta 3");


            ad.Imprimir();
            System.out.println("Tamaño actual: " + ad.size());
            ad.crearCarpeta("root/Alfombra 1", "Alfombra 4");
            ad.crearCarpeta("root/Alfombra 2", "Alfombra 2");
            ad.crearCarpeta("raiz/Alfombra 2", "Alfombra 5");
            ad.crearCarpeta("root/Alfombra 2", "Alfombra 4");

            ad.Imprimir();
            System.out.println("Tamaño actual: " + ad.size());
            ad.mover("raíz/Alfombra 2", "raíz/Alfombra 1");
            ad.mover("raíz/Alfombra 2", "raíz/Alfombra 3");
            ad.Imprimir();
            System.out.println("Tamaño actual: " + ad.size());
            ad.eliminarCarpeta("root/Carpeta 1");
            ad.eliminarCarpeta("root/Carpeta 3/Carpeta 2");
            ad.Imprimir();
            System.out.println("Tamaño actual: " + ad.size());
        }

}
