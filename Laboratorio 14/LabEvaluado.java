package labevaluado;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 Integrantes : 
 ;Samir Alarcón Hernández
 ;Brian Cadiz Contreras
 ;Darío Catalán Mora
 */

/*
 Ejercicio ---- 1557. Número mínimo de vértices para llegar a todos los nodos---

 Dado un gráfico acíclico dirigido, con n vértices numerados de 0 a n-1, y una matriz de aristas donde aristas[i] = [desdei, hastai] representa una arista dirigida desde el nodo desdei hasta el nodo hastai.

 Encuentre el conjunto más pequeño de vértices desde el cual se pueden alcanzar todos los nodos en el gráfico. Está garantizado que existe una solución única.

 Tenga en cuenta que puede devolver los vértices en cualquier orden.

 */
public class LabEvaluado {

    //Analizando ambos ejemplos del enunciado notamos que el output o la salida corresponde a todos aquellos vértices que no tienen aristas entrando en ellos, por lo que esos son los nodos o vértices que hay que retornar
    public List<Integer> findSmallestSetOfVertices(int n, List<List<Integer>> edges) {

        Map<Integer, Integer> grado = new HashMap(); //Asignamos el HashMap, el cual nos va a ayudar a rastrear cuántos vértices tienen un grado de entrada igual a cero, 
        for (List<Integer> edge : edges) {//A través del for, recorremos todos las aristas
            int fin = edge.get(1);//Como dice el enunciado, el array de aristas, el primer índice corresponde al valor más pequeño el cual es el punto de partida, y el punto final (de llegada)es el segundo, el cual es el que nos interesa  
            grado.put(fin, grado.getOrDefault(fin, 0) + 1);//Actualizamos el grado
        }
        List<Integer> resultado = new ArrayList<>();//Asignamos la lista donde almacenaremos los vértices que cumplen con la condición solicitada
        for (int i = 0; i < n; i++) {//Consideramos el parámetro n, el cual nos indica que existen n vértices en el grafo, asignado como n-1
            if (!grado.containsKey(i)) {//Si no contiene el valor de i, el grado será si o si cero, que es lo que buscamos
                resultado.add(i);//Los vértices encontrados los añadimos a la lista
            }
        }
        return resultado;//Retornamos la lista
    }
    
    
    /*
    Fuentes : 
    https://leetcode.ca/2020-03-05-1557-Minimum-Number-of-Vertices-to-Reach-All-Nodes/
    https://www.geeksforgeeks.org/smallest-set-of-vertices-to-visit-all-nodes-of-the-given-graph/
    https://medium.com/the-brainwave/minimum-number-of-vertices-to-reach-all-nodes-by-graph-theory-with-python-and-javascript-642b8134799e
    */

    
}
