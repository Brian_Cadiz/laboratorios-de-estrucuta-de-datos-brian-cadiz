import java.util.Stack;

public class Parsing {
    public static String fromInfijoToPostfijo(String infijo) {
        Stack<Character> simbol=new Stack<>();
        char[] charinfijo;
        charinfijo=infijo.toCharArray();
        String salida="";

        for (char x:charinfijo){
            if(isANumber(x)){
                salida+=x;
            }else if(x=='('){
                    simbol.push(x);
                }else if(x==')'){
                String stack="";
                char[]ArrPila=stack.toCharArray();

                while (simbol.peek()!='('){
                    stack+=simbol.pop();
                }
                for (char c:ArrPila){
                    simbol.push(c);
                }
            }else if (!isANumber(x)){
                if (simbol.size()==0){
                    simbol.push(x);
                }else{
                    if (prioridad(x)>prioridad((simbol.peek()))){
                        simbol.push(x);
                    }else {
                        salida+=simbol.pop();
                        simbol.push(x);
                    }
                }
            }

        }
        while (simbol.empty()==false){
            salida+=simbol.pop();
        }
        return salida;
    }

    private static boolean isANumber(char symbol) {
        return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' ||
                symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' ||
                symbol == '8' || symbol == '9';
    }

    private static int prioridad(char symbol) {
        if(symbol == '(') return 0;
        if(symbol == '+' || symbol == '-') return 1;
        if(symbol == '*' || symbol == '/') return 2;
        return -1;
    }


    public static ArbolExpresion getArbol(String postfijo) {
        Stack<ArbolExpresion> a=new Stack<>();
        char[]charPostFijo=postfijo.toCharArray();
        for (char x:charPostFijo) {
            ArbolExpresion arbol2 = new ArbolExpresion(x);

            if (isANumber(x)){
                a.push(arbol2);
            }else{
                arbol2.left=a.pop();
                arbol2.right=a.pop();
                a.push(arbol2);
            }
        }
        return a.peek();
    }


}
