import javafx.scene.input.DataFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.SimpleFormatter;

public class ClassDate {

    /*
     * datos --> 32 bits
     * [16,20] --> 5 bits --> Dia
     * [12,15] --> 4 bits --> Mes
     * [0,11] --> 12 bits --> Año
     */
    private int dato;
    private static ClassDate instance=null;

    public static ClassDate getInstance(){
        if(instance==null){
            instance=new ClassDate();
        }
        return instance;
    }

    public ClassDate() {

        dato= 0;
    }


    public int getAño() {
         int dia = dato >>> 20;
        return dia;
    }

    public int getMes() {
        int mes = (dato << 12) >>> 28;
        return mes;
    }

    public int getDia() {
        int dia = (dato << 16) >>> 27;
        return dia ;
    }

    public int getHora(){
        int hora=(dato<<21)>>>27;
        return hora;
    }

    public int getMinuto(){
        int minuto=(dato<<26)>>>26;
        return minuto;
    }


    // para el set año el dato ingresado debe de ser propio de un año por lo que el dato ingresado debe de ser  mayor que 0 y menor que calquier numero pero yo le puse como limite 9999
    //se usa como mascara el numero binario 00000000000011111111111111111111
    public void setAño(int año) {
        if(año >= 0 && año < 9999) {

             int mascara = 1048575;
            dato = (dato & mascara) | (año << 20);
            System.out.println("DATOS: "+Long.toBinaryString(dato));
        }else  {
            System.out.println("Año no ingresada");
        }

    }

    // para el set mes el dato ingresado debe de ser propio de un mes por lo que el dato ingresado debe de ser  mayor que 0 y menor que 13
   //la mascara usada es el numero binario 01111111111100001111111111111111
    public void setMes(int mes) {
        if(mes > 0 && mes < 13) {

             int mascara = 2146500607 |(1<<31);
            dato = (dato & mascara) | (mes << 16);
            System.out.println("DATOS: "+Long.toBinaryString(dato));
        }else  {
            System.out.println("Mes no igresada");
        }
    }

    // para el set mes el dato ingresado debe de ser propio de un dia por lo que el dato ingresado debe de ser  mayor que 0 y menor que 32
    //la mascara usada es el numero binario 01111111111111110000011111111111
    public void setDia(int dia) {
        if(dia >0 && dia < 32) {

             int mascara = 2147420159 | (1<<31);
            dato=(dato & mascara) | (dia << 11);
            System.out.println("DATOS: "+Long.toBinaryString(dato));
        }else{
            System.out.println("dia no ingresado");
        }
    }

    // para el set hora el dato ingresado debe de ser propio de una hora por lo que el dato ingresado debe de ser  mayor que 0 y menor que 24
   //la mascara usada es el numero binario 01111111111111111111100000111111
    public void setHora(int hora) {
        if(hora >= 0 && hora < 24) {

            int mascara = 2147481663 | (1<<31);
            dato = (dato & mascara) | (hora << 6);
            System.out.println("DATOS: "+Long.toBinaryString(dato));
        }else  {
            System.out.println("hora no ingresada");
        }

    }

    // para el set minuto el dato ingresado debe de ser propio de un minuto por lo que el dato ingresado debe de ser  mayor o igual que 0 y menor que 60
    //la mascara usada es el numero binario 01111111111111111111111111000000
    public void setMinuto(int minuto) {
        if(minuto >= 0 && minuto < 60) {
            // Para limpiar el valor anterior
            int mascara = 2147483584| (1<<31);
            dato = (dato & mascara) | minuto;
            System.out.println("DATOS: "+Long.toBinaryString(dato));
        }else  {
            System.out.println("minuto no ingresada");
        }

    }
    @Override
    public String toString() {

        return  getDia()+"/"+getMes()+"/"+getAño()+"     "+"hora:"+ getHora()+":"+getMinuto();

    }
}
