public class ParenthesesDepth {
    public int maxDepth(String s) {
       //el int depth hace referencia a la profundidad de los parentesis y el int open hace referencia si el parentesis esta habierto, si el paretesis esta abierto se aumenta  y si el paretesis esta cerrado entonces se desminuye.
        int depth=0;
        int open=0;

        for(char x: s.toCharArray()){
            if(x =='('){
                open++;
            }
            if(x ==')'){
                open--;
            }
            depth=Math.max(depth,open);
        }
        return depth;
    }
}
