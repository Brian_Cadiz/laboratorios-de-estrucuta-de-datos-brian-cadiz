import java.util.Arrays;

public class MaxHap {
    private int length = 10;
    private int size = 0;
    private int [] items = new int[length];

    public MaxHap() {

    }

    public MaxHap(int [] A) {
        // Build heap
        items = A;
        size = A.length;
        length = A.length;
        buildHeap();

        duplicateCapacity();
    }

    public void printHeap() {
        for(int i = 0; i< size; i++) {
            System.out.print(items[i] + " ");
        }
        System.out.println();
    }

    public void insert(int item) {
        // insert a new element
        duplicateCapacity();
        items[size] = item;
        size++;
        heapifyUp(size-1);
    }

    public int top() {
        // get items[0]
        if(size > 0)	return items[0];
        else return -1;
    }

    public int delete() {
        // get items[0] and delete it
        if(size > 0) {
            int item = items[0];
            items[0] = items[size-1];
            size--;
            heapify(0);
            return item;
        }
        return -1;
    }

    private void duplicateCapacity(){
        // duplicate the capacity of the array
        if(size == length) {
            items = Arrays.copyOf(items, length*2);
            length *= 2;
        }
    }

    private void heapifyUp(int i) {
        int padre=parent(i);
        if(items[i]>items[padre]){
            swap(i,padre);
            heapifyUp(padre);
        }
    }
    private boolean Hoja(int x){
        if(x>(size/2)&& x<=size){
            return true;
        }
        return false;
    }

    private void heapify(int i) {
        if(Hoja(i)==true){
            return;
        }
        int rigth=rightChild(i);
        int izquierda=leftChild(i);

        if (items[i]<items[izquierda]||items[i]<items[rigth]){
            int grande=0;
            if(izquierda<=size && items[izquierda] >items[i] ){
               grande=izquierda;
            }else {
                grande=i;
            }
            if(rigth<=size && items[rigth]>items[grande]){
                grande=rigth;
            }
            if (grande!=i){
                swap(i,grande);
            }
            heapifyUp(grande);
        }
    }

    private void buildHeap() {
        // Given an array, get the heap
        for(int i = size/2; i >= 0; i--) {
            heapify(i);
        }
    }

    private int parent(int i) {
        return (i-1)/2;
    }
    private int leftChild(int i) {
        return 2*i+1;
    }
    private int rightChild(int i) {
        return 2*i+2;
    }
    private void swap(int i, int j) {
        int temp = items[i];
        items[i] = items[j];
        items[j] = temp;
    }
    public static void main(String[] args) {
        MaxHap maxh=new MaxHap();
        maxh.insert(5);
        maxh.insert(2);
        maxh.insert(11);
        maxh.insert(20);
        maxh.buildHeap();
        maxh.printHeap();
    }

}
